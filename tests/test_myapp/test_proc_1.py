import pytest
from poetry_test.myproc import *

@pytest.fixture
def dummy():
    print('fixture dummy')
    return True


def test_proc1_1(dummy):
    assert 1 == proc1(0)

def test_proc1_2(dummy):
    assert 2 == proc1(1)


def test_proc2_1(mocker):
    mocker.patch('poetry_test.myproc.sub_func', return_value=0)
    assert 0 == proc2()

def test_proc2_2(mocker):
    mocker.patch('poetry_test.myproc.sub_func', return_value=1)
    assert 1 == proc2()

def test_proc2_3(mocker):
    # mockなし
    assert 1 == proc2()
